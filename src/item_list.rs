use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

use crate::dmm::{ApiResult, ElementVec};

#[derive(Serialize, Debug, Default)]
pub struct ItemListParams {
    pub site: SiteValue,
    pub service: Option<String>,
    pub floor: Option<String>,
    pub hits: Option<i64>,
    pub offset: Option<i64>,
    pub sort: Option<SortValue>,
    pub keyword: Option<String>,
    pub cid: Option<String>,
    pub article: Option<ArticleValue>,
    pub article_id: Option<String>,
    pub gte_date: Option<NaiveDateTime>,
    pub lte_date: Option<NaiveDateTime>,
    pub mono_stock: Option<MonoStockValue>,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum SortValue {
    Rank,
    Price,
    #[serde(rename = "-price")]
    PriceDesc,
    Date,
    Review,
    Match,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum ArticleValue {
    Actress,
    Author,
    Genre,
    Series,
    Maker,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "snake_case")]
pub enum MonoStockValue {
    Stock,
    Reserve,
    ReserveEmpty,
    Mono,
    Dmp,
}

#[derive(Serialize, Debug, Default)]
pub enum SiteValue {
    #[serde(rename = "FANZA")]
    Fanza,
    #[default]
    #[serde(rename = "DMM.com")]
    Dmm,
}

#[derive(Deserialize, Debug)]
pub struct ItemListResult {
    pub status: i64,
    pub result_count: i64,
    pub total_count: i64,
    pub first_position: i64,
    pub items: ElementVec<Item>,
}
impl ApiResult for ItemListResult {}

#[derive(Deserialize, Debug)]
// #[serde(deny_unknown_fields)]
pub struct Item {
    pub service_code: String,
    pub service_name: String,
    pub floor_code: String,
    pub floor_name: String,
    pub category_name: String,
    pub content_id: String,
    pub product_id: String,
    pub title: String,
    pub volume: Option<String>,
    pub number: Option<String>,
    pub review: Option<Review>,
    #[serde(rename = "URL")]
    pub url: String,
    #[serde(rename = "affiliateURL")]
    pub affiliate_url: Option<String>,
    #[serde(rename = "imageURL")]
    pub image_url: ImageUrl,
    pub tachiyomi: Option<Tachiyomi>,
    #[serde(rename = "sampleImageURL")]
    pub sample_image_url: Option<SampleImageUrl>,
    #[serde(rename = "sampleMovieURL")]
    pub sample_movie_url: Option<SampleMovieUrl>,
    pub prices: Prices,
    pub date: String, // datetime?
    pub iteminfo: Iteminfo,
    pub cdinfo: Option<Cdinfo>,
    pub jancode: Option<String>,
    pub maker_product: Option<String>,
    pub isbn: Option<String>,
    // pub stock: Option<StockValue>,
    pub stock: Option<String>, // dmm: found return '1'
    pub directory: Option<ElementVec<Directory>>,
    pub campaign: Option<Campaign>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "snake_case")]
pub enum StockValue {
    Stock,
    Reserve,
    ReserveEmpty,
    Mono,
    Dmp,
    Empty,
}

#[derive(Deserialize, Debug)]
pub struct Review {
    pub count: i64,
    pub average: f64,
}

#[derive(Deserialize, Debug)]
pub struct ImageUrl {
    pub list: String,
    pub small: String,
    pub large: String,
}

#[derive(Deserialize, Debug)]
pub struct Tachiyomi {
    #[serde(rename = "URL")]
    pub url: String,
    #[serde(rename = "affiliateURL")]
    pub affiliate_url: String,
}

#[derive(Deserialize, Debug)]
pub struct SampleImageUrl {
    pub sample_s: SampleS,
    pub sample_l: Option<SampleL>,
}

#[derive(Deserialize, Debug)]
pub struct SampleS {
    pub image: ElementVec<String>,
}
#[derive(Deserialize, Debug)]
pub struct SampleL {
    pub image: ElementVec<String>,
}

#[derive(Deserialize, Debug)]
pub struct SampleMovieUrl {
    pub size_476_306: String,
    pub size_560_360: String,
    pub size_644_414: String,
    pub size_720_480: String,
    pub pc_flag: i64,
    pub sp_flag: i64,
}

#[derive(Deserialize, Debug)]
pub struct Prices {
    pub price: String,
    pub list_price: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct Deliveries {
    pub delivery: ElementVec<Delivery>,
}

#[derive(Deserialize, Debug)]
pub struct Delivery {
    pub r#type: String,
    pub price: String,
}

#[derive(Deserialize, Debug)]
pub struct Iteminfo {
    pub genre: Option<ElementVec<Genre>>,
    pub series: Option<ElementVec<Series>>,
    pub maker: Option<ElementVec<Maker>>,
    pub actor: Option<ElementVec<Actor>>,
    pub actress: Option<ElementVec<Actress>>,
    pub director: Option<ElementVec<Director>>,
    pub author: Option<ElementVec<Author>>,
    pub label: Option<ElementVec<Label>>,
    pub r#type: Option<ElementVec<Type>>,
    pub color: Option<ElementVec<Color>>,
    pub size: Option<ElementVec<Size>>,
}

#[derive(Deserialize, Debug)]
pub struct Genre {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Series {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Maker {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Actor {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Actress {
    pub name: String,
    pub ruby: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Director {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Author {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Label {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Type {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Color {
    pub name: String,
    pub id: String,
}
#[derive(Deserialize, Debug)]
pub struct Size {
    pub name: String,
    pub id: String,
}

#[derive(Deserialize, Debug)]
pub struct Cdinfo {
    pub king: String,
}

#[derive(Deserialize, Debug)]
pub struct Directory {
    pub id: String,
    pub name: String,
}

#[derive(Deserialize, Debug)]
pub struct Campaign {
    pub date_begin: Option<NaiveDateTime>,
    pub date_end: Option<NaiveDateTime>,
    pub title: Option<String>,
}
