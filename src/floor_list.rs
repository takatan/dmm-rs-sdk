use crate::dmm::{ApiResult, ElementVec};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Debug, Default)]
pub struct FloorListParams {}

#[derive(Deserialize, Debug)]
pub struct FloorListResult {
    pub site: ElementVec<Site>,
}
impl ApiResult for FloorListResult {}

#[derive(Deserialize, Debug)]
pub struct Site {
    pub name: String,
    pub code: String,
    pub service: ElementVec<Service>,
}

#[derive(Deserialize, Debug)]
pub struct Service {
    pub name: String,
    pub code: String,
    pub floor: ElementVec<Floor>,
}
#[derive(Deserialize, Debug)]
pub struct Floor {
    pub id: String,
    pub name: String,
    pub code: String,
}
