use crate::dmm::{ApiResult, ElementVec};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Debug, Default)]
pub struct SeriesSearchParams {
    pub floor_id: String,
    pub initial: Option<char>,
    pub hits: Option<i64>,
    pub offset: Option<i64>,
}

#[derive(Deserialize, Debug)]
pub struct SeriesSearchResult {
    pub status: String,
    pub result_count: i64,
    pub total_count: i64,
    pub first_position: i64,
    pub site_name: String,
    pub site_code: String,
    pub service_name: String,
    pub service_code: String,
    pub floor_id: String,
    pub floor_name: String,
    pub floor_code: String,
    pub series: ElementVec<Series>, // FIXME: empty case
}
impl ApiResult for SeriesSearchResult {}

#[derive(Deserialize, Debug)]
pub struct Series {
    pub series_id: String,
    pub name: String,
    pub ruby: String,
    pub list_url: String,
}
